FROM steamcmd/steamcmd:alpine
RUN apk add --update make python3 git curl
WORKDIR /root/build
COPY . .
