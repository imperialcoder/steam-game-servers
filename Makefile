STEAM_APP_DIR := /root/.steam/SteamApps
STEAM_COMPAT_DIR := $(STEAM_APP_DIR)/compatdata
STEAM_PROTON := $(STEAM_APP_DIR)/common/Proton\ -\ Experimental/proton

all:

up:
	docker-compose up -d

down:
	docker-compose down -t 0

build:
	docker-compose build --no-cache

exec:
	docker-compose run -d --rm --name steamcmd_exec steamcmd 
	docker exec -it steamcmd_exec /bin/sh

login:
	steamcmd +login skaterjess +quit

proton:
	steamcmd +login anonymous +app_update 1493710 validate +quit

astroneer:
	steamcmd +@sSteamCmdForcePlatformType windows +login anonymous +app_update 728470 validate +quit

proton-caller:
	curl -O https://github.com/caverym/proton-caller/releases/download/3.1.0/proton-caller_3.1.0_amd64.tar.xz 
	tar xf proton-caller*.xz --directory=proton-caller
